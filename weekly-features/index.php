<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="./css/style.css">
	<link rel="stylesheet" type="text/css" href="./css/file-upload.css">
	<script src="./js/jquery-3.2.1.min.js"></script>
	<script src="./js/file-upload.js"></script>
	<script type="text/javascript">
        $(document).ready(function() {
            $('.file-upload').file_upload();
        });
    </script>
</head>
<body style="background-color: #fff;">
	<div class="form-group">
		<label class="file-upload btn btn-primary">
        	Browse for image ... <input type="file" id="file1" name="file1" />
    	</label>
	</div>
	
	<div class="container" style="text-align: center;">
		<button type="button" class="btn btn-primary main" style="width: 100%;">Process</button>
	</div>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="./js/main.js"></script>
</body>
</html>