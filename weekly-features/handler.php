<?php
$uploadedfile = $_FILES;
$excelFile = $uploadedfile['csvfile']['tmp_name'];

require('XLSXReader.php');
$xlsx = new XLSXReader($excelFile);
$sheetNames = $xlsx->getSheetNames();
$counter = 0;
$bool = false;

$localdebug = 1; // change to 1 if currently on local
$pathname = './generated/';
$response = [];
$uploadedfile = $_FILES;
$monthname = 'april';

if (!file_exists( $pathname . $monthname )) {
    mkdir( $pathname . strtolower($monthname) , 0777, true);
}

$files = glob( $pathname . $monthname . '/*' );

foreach($files as $file){ // iterate files
 	if(is_file($file)){
    	unlink($file); // delete file
 	}
 	else{
 		system('rm -rf ' . escapeshellarg($file));
 	}
}

while( $bool != true ){
	isset( $sheetNames[$counter] ) ? $bool = true : $counter++;
}

$sheet = $xlsx->getSheet($sheetNames[$counter]);
$data = $sheet->getData();

$start = 6;
$end = 40;
$week = 1;
$startOfWeek = 1;

system('cp -R ' . escapeshellarg('./template') . ' ' . escapeshellarg( $pathname . $monthname . '/week-' . $week ) );

for( $x = $start; $x <= $end; $x++ ){
	if( $x == 14 || $x == 23 || $x == 32 ){
		$startOfWeek = 1;
		$week++;
		system('cp -R ' . escapeshellarg('./template') . ' ' . escapeshellarg( $pathname . $monthname . '/week-' . $week ) );
	}
	else{
		$ads = file_get_contents( $pathname . $monthname . '/week-' . $week . '/ads.html' );
		$title = ucfirst( $monthname ) . ' 2018 Week ' . $week . ' Ads';

		$ads = str_replace( '{{title}}', $title, $ads );
		$ads = str_replace( '{{description' . $startOfWeek . '}}', $data[$x][5], $ads );
		$ads = str_replace( '{{urltext' . $startOfWeek . '}}', $data[$x][7], $ads );
		$ads = str_replace( '{{alt' . $startOfWeek . '}}', $data[$x][4], $ads );

		if( $localdebug ){
			if( file_exists( './images/' . $data[$x][4] . '.jpg' ) ){
				$imgurl = './img/' . $data[$x][4] . '.jpg';
				system('cp ' . escapeshellarg( './images/' . $data[$x][4] . '.jpg' ) . ' ' . escapeshellarg( $pathname . $monthname .'/week-' . $week . '/img/'. $data[$x][4] . '.jpg' ) );
			}
			if( file_exists( './images/' . $data[$x][4] . '_80x80.jpg' ) ){
				system('cp ' . escapeshellarg( './images/' . $data[$x][4] . '_80x80.jpg' ) . ' ' . escapeshellarg( $pathname . $monthname .'/week-' . $week . '/img/'. $data[$x][4] . '_80x80.jpg' ) );
				$imgurl = './img/' . $data[$x][4] . '_80x80.jpg';
			}

			$css = './css/style.css';
			$tilogo = './img/ti-logo.png'
		}
		else{
			if( file_exists( './images/' . $data[$x][4] . '.jpg' ) ){
				system('cp ' . escapeshellarg( './images/' . $data[$x][4] . '.jpg' ) . ' ' . escapeshellarg( $pathname . $monthname .'/week-' . $week . '/img/'. $data[$x][4] . '.jpg' ) );
				$imgurl = 'https://ads.aspencore.com/weekly-features/ti/2018/' . $monthname . '/week-'. $week .'/img/'. $data[$x][4] .'.jpg';
			}
			if( file_exists( './images/' . $data[$x][4] . '_80x80.jpg' ) ){
				system('cp ' . escapeshellarg( './images/' . $data[$x][4] . '_80x80.jpg' ) . ' ' . escapeshellarg( $pathname . $monthname .'/week-' . $week . '/img/'. $data[$x][4] . '_80x80.jpg' ) );
				$imgurl = 'https://ads.aspencore.com/weekly-features/ti/2018/' . $monthname . '/week-'. $week .'/img/'. $data[$x][4] .'_80x80.jpg';
			}

			$css = 'https://ads.aspencore.com/weekly-features/ti/2018/'. $monthname . '/week-'. $week .'/css/style.css';
			$tilogo = 'https://ads.aspencore.com/weekly-features/ti/2018/'. $monthname .'/week-'. $week .'/img/ti-logo.png'
		}

		$ads = str_replace( '{{tilogo}}', $tilogo, $ads );
		$ads = str_replace( '{{imageurl' . $startOfWeek . '}}', $imgurl, $ads );
		$ads = str_replace( '{{css}}', $css, $ads );
		file_put_contents($pathname . $monthname . '/week-' . $week . '/ads.html', $ads );

		$startOfWeek++;
	}
}

?>