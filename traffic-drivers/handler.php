<?php
	
	generateForLocal();

	function generateForLocal()
	{
		$pathname = './generated/';
	
		$response = [];
		$uploadedfile = $_FILES;

		$data = json_decode( $_POST['data'], true);
		$monthname = $_POST['monthname'];

		if( !$monthname ){
			die;
		}

		if (!file_exists( $pathname . $monthname )) {
		    mkdir( $pathname . strtolower($monthname) , 0777, true);
		}

		$files = glob( $pathname . $monthname . '/*' );

		foreach($files as $file){ // iterate files
		 	if(is_file($file)){
		    	unlink($file); // delete file
		 	}
		 	else{
		 		system('rm -rf ' . escapeshellarg($file));
		 	}
		}

		$counter = 1;
		foreach ( $data as $arr ) {
			if( count( $arr ) ){
				system('cp -R ' . escapeshellarg('./template') . ' ' . escapeshellarg( $pathname . $monthname . '/' . strtolower($arr['foldername']) ) );

				$ad1 = file_get_contents( $pathname . $monthname . '/' . $arr['foldername'] . '/ad-1/ads.html' );
				$ad1 = str_replace( '{{monthname}}', $monthname, $ad1 );
				$ad1 = str_replace( '{{foldername}}', $arr['foldername'], $ad1 );
				$ad1 = str_replace( '{{headline}}', $arr['headline'], $ad1 );
				$ad1 = str_replace( '{{description}}', $arr['description'], $ad1 );
				$ad1 = str_replace( '{{urltext}}', $arr['urltext'], $ad1 );
				system('cp ' . escapeshellarg( $uploadedfile['file' . $counter]['tmp_name'] ) . ' ' . escapeshellarg( $pathname . $monthname .'/'. $arr['foldername'] .'/ad-1/img/'. $uploadedfile['file' . $counter]['name'] ) );
				
				$imgurl = './img/'. $uploadedfile['file' . $counter]['name'];
				$css = './css/style.css';

				$ad1 = str_replace( '{{imageurl}}', $imgurl, $ad1 );
				$ad1 = str_replace( '{{css}}', $css, $ad1 );
				file_put_contents($pathname . $monthname . '/' . $arr['foldername'] . '/ad-1/ads.html', $ad1 );


				$ad2 = file_get_contents( $pathname . $monthname . '/' . $arr['foldername'] . '/ad-2/ads.html' );
				$ad2 = str_replace( '{{monthname}}', $monthname, $ad2 );
				$ad2 = str_replace( '{{foldername}}', $arr['foldername'], $ad2 );
				$ad2 = str_replace( '{{headline}}', $arr['headline'], $ad2 );
				$ad2 = str_replace( '{{description}}', $arr['description'], $ad2 );
				$ad2 = str_replace( '{{urltext}}', $arr['urltext'], $ad2 );

				system('cp ' . escapeshellarg( $uploadedfile['file' . $counter]['tmp_name'] ) . ' ' . escapeshellarg( $pathname . $monthname .'/'. $arr['foldername'] .'/ad-2/img/'. $uploadedfile['file' . $counter]['name'] ) );

				$imgurl = './img/'. $uploadedfile['file' . $counter]['name'];
				$css = './css/style2.css';

				$ad2 = str_replace( '{{imageurl}}', $imgurl, $ad2 );
				$ad2 = str_replace( '{{css}}', $css, $ad2 );

				file_put_contents($pathname . $monthname . '/' . $arr['foldername'] . '/ad-2/ads.html', $ad2 );
				
				$response[$counter] = array(
					'link1' => $pathname . $monthname . '/' . $arr['foldername'] . '/ad-1/ads.html'
					,'link2' => $pathname . $monthname . '/' . $arr['foldername'] . '/ad-2/ads.html'
				);
			}
			$counter++;
		}

		generateForLive();

		die(json_encode($response));
	}

	function generateForLive()
	{
		$pathname = './generated/';
		$uploadedfile = $_FILES;
		$data = json_decode( $_POST['data'], true);
		$monthname = $_POST['monthname'];
		$counter = 1;

		if (!file_exists( $pathname . $monthname . '-forlive')) {
		    mkdir( $pathname . strtolower($monthname) . '-forlive', 0777, true);
		}

		foreach ( $data as $arr ) {
			if( count( $arr ) ){
				system('cp -R ' . escapeshellarg('./template') . ' ' . escapeshellarg( $pathname . $monthname . '-forlive/' . strtolower($arr['foldername']) ) );

				$ad1 = file_get_contents( $pathname . $monthname . '-forlive/' . $arr['foldername'] . '/ad-1/ads.html' );
				$ad1 = str_replace( '{{monthname}}', $monthname, $ad1 );
				$ad1 = str_replace( '{{foldername}}', $arr['foldername'], $ad1 );
				$ad1 = str_replace( '{{headline}}', $arr['headline'], $ad1 );
				$ad1 = str_replace( '{{description}}', $arr['description'], $ad1 );
				$ad1 = str_replace( '{{urltext}}', $arr['urltext'], $ad1 );
				system('cp ' . escapeshellarg( $uploadedfile['file' . $counter]['tmp_name'] ) . ' ' . escapeshellarg( $pathname . $monthname .'-forlive/'. $arr['foldername'] .'/ad-1/img/'. $uploadedfile['file' . $counter]['name'] ) );
				
				$imgurl = 'https://ads.aspencore.com/traffic-drivers/ti/2018/'. strtolower( $monthname ) .'/'. strtolower($arr['foldername']) .'/ad-1/img/'. $uploadedfile['file' . $counter]['name'];
				$css = 'https://ads.aspencore.com/traffic-drivers/ti/2018/'. strtolower( $monthname ) .'/'. strtolower($arr['foldername']) .'/ad-1/css/style.css';

				$ad1 = str_replace( '{{imageurl}}', $imgurl, $ad1 );
				$ad1 = str_replace( '{{css}}', $css, $ad1 );
				file_put_contents($pathname . $monthname . '-forlive/' . $arr['foldername'] . '/ad-1/ads.html', $ad1 );


				$ad2 = file_get_contents( $pathname . $monthname . '-forlive/' . $arr['foldername'] . '/ad-2/ads.html' );
				$ad2 = str_replace( '{{monthname}}', $monthname, $ad2 );
				$ad2 = str_replace( '{{foldername}}', $arr['foldername'], $ad2 );
				$ad2 = str_replace( '{{headline}}', $arr['headline'], $ad2 );
				$ad2 = str_replace( '{{description}}', $arr['description'], $ad2 );
				$ad2 = str_replace( '{{urltext}}', $arr['urltext'], $ad2 );

				system('cp ' . escapeshellarg( $uploadedfile['file' . $counter]['tmp_name'] ) . ' ' . escapeshellarg( $pathname . $monthname .'-forlive/'. $arr['foldername'] .'/ad-2/img/'. $uploadedfile['file' . $counter]['name'] ) );

				$imgurl = 'https://ads.aspencore.com/traffic-drivers/ti/2018/'. strtolower( $monthname ) .'/'. strtolower($arr['foldername']) .'/ad-2/img/'. $uploadedfile['file' . $counter]['name'];
				$css = 'https://ads.aspencore.com/traffic-drivers/ti/2018/'. strtolower( $monthname ) .'/'. strtolower($arr['foldername']) .'/ad-2/css/style2.css';

				$ad2 = str_replace( '{{imageurl}}', $imgurl, $ad2 );
				$ad2 = str_replace( '{{css}}', $css, $ad2 );

				file_put_contents($pathname . $monthname . '-forlive/' . $arr['foldername'] . '/ad-2/ads.html', $ad2 );
			}
			$counter++;
		}
	}

?>