<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="./css/style.css">
	<link rel="stylesheet" type="text/css" href="./css/file-upload.css">
	<script src="./js/jquery-3.2.1.min.js"></script>
	<script src="./js/file-upload.js"></script>
	<script type="text/javascript">
        $(document).ready(function() {
            $('.file-upload').file_upload();
        });
    </script>
</head>
<body style="background-color: #fff;">
		<div class="container" style="text-align: center;">
			<div class="form-group">
			  <label for="monthname">Month Name</label>
			  <input type="text" class="form-control" id="monthname">
			</div>
		</div>

		<div class="container-fluid">
		  <div class="row">
		    <div class="col-md-3 first">
				<div class="form-group">
				  <label for="foldername">Folder Name</label>
				  <input type="text" class="form-control" id="foldername">
				</div>
				<div class="form-group">
				  <label for="headline">Headline</label>
				  <input type="text" class="form-control" id="headline">
				</div>
				<div class="form-group">
				  <label for="description">Text</label>
				  <textarea class="form-control" rows="5" id="description"></textarea>
				</div>
				<div class="form-group">
					<label class="file-upload btn btn-primary">
                    	Browse for image ... <input type="file" id="file1" name="file1" />
                	</label>
				</div>
				<div class="form-group">
				  <label for="urltext">URL</label>
				  <input type="text" class="form-control" id="urltext">
				</div>
				<span class="link1 1"></span>
				<span class="link2 1"></span>
		    </div>

		    <div class="col-md-3 second">
		      	<div class="form-group">
				  <label for="foldername">Folder Name</label>
				  <input type="text" class="form-control" id="foldername">
				</div>
				<div class="form-group">
				  <label for="headline">Headline</label>
				  <input type="text" class="form-control" id="headline">
				</div>
				<div class="form-group">
				  <label for="description">Text</label>
				  <textarea class="form-control" rows="5" id="description"></textarea>
				</div>
				<div class="form-group">
					<label class="file-upload btn btn-primary">
                    	Browse for image ... <input type="file" id="file2" />
                	</label>
				</div>
				<div class="form-group">
				  <label for="urltext">URL</label>
				  <input type="text" class="form-control" id="urltext">
				</div>
				<span class="link1 2"></span>
				<span class="link2 2"></span>
		    </div>

		    <div class="col-md-3 third">
		      	<div class="form-group">
				  <label for="foldername">Folder Name</label>
				  <input type="text" class="form-control" id="foldername">
				</div>
				<div class="form-group">
				  <label for="headline">Headline</label>
				  <input type="text" class="form-control" id="headline">
				</div>
				<div class="form-group">
				  <label for="description">Text</label>
				  <textarea class="form-control" rows="5" id="description"></textarea>
				</div>
				<div class="form-group">
					<label class="file-upload btn btn-primary">
                    	Browse for image ... <input type="file" id="file3" />
                	</label>
				</div>
				<div class="form-group">
				  <label for="urltext">URL</label>
				  <input type="text" class="form-control" id="urltext">
				</div>
				<span class="link1 3"></span>
				<span class="link2 3"></span>
		    </div>

		    <div class="col-md-3 fourth">
		      	<div class="form-group">
				  <label for="foldername">Folder Name</label>
				  <input type="text" class="form-control" id="foldername">
				</div>
				<div class="form-group">
				  <label for="headline">Headline</label>
				  <input type="text" class="form-control" id="headline">
				</div>
				<div class="form-group">
				  <label for="description">Text</label>
				  <textarea class="form-control" rows="5" id="description"></textarea>
				</div>
				<div class="form-group">
					<label class="file-upload btn btn-primary">
                    	Browse for image ... <input type="file" id="file4" />
                	</label>
				</div>
				<div class="form-group">
				  <label for="urltext">URL</label>
				  <input type="text" class="form-control" id="urltext">
				</div>
				<span class="link1 4"></span>
				<span class="link2 4"></span>
		    </div>
		  </div>
		</div>

		<div class="container" style="text-align: center;">
			<button type="button" class="btn btn-primary main" style="width: 100%;">Process</button>
		</div>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="./js/main.js"></script>
</body>
</html>