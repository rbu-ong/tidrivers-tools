$( document ).ready(function() {
    $(".main").click(function(){

    	var inputs = new Array();
    	var monthname = $('#monthname')[0].value;

    	inputs[1] = $(".first :input");
    	inputs[2] = $(".second :input");
    	inputs[3] = $(".third :input");
    	inputs[4] = $(".fourth :input");
    	var data = {};

    	for( y = 1; y <= 4; y++ ){
    		var temp1 = {};

    		for( x = 0; x < inputs[y].length; x++ ){
	    		if( inputs[y][x].id ){
	    			temp1[inputs[y][x].id] = inputs[y][x].value;
	    		}
	    	}

	    	data[y] = temp1
    	}

        var fileupload = new FormData();
        fileupload.append('file1', $('#file1').prop('files')[0]);
        fileupload.append('file2', $('#file2').prop('files')[0]);
        fileupload.append('file3', $('#file3').prop('files')[0]);
        fileupload.append('file4', $('#file4').prop('files')[0]);

        fileupload.append('data', JSON.stringify(data));
        fileupload.append('monthname', monthname);
    	
    	$.ajax({
    		url: "./handler.php"
    		,type: 'POST'
    		,data: fileupload
            ,cache: false
            ,contentType: false
            ,processData: false
    		,success: function(result){
	        	result = JSON.parse(result);
                for( x = 1; x <= 4; x++ ){
                    $('.link1.' + x).children().remove();
                    $('.link2.' + x).children().remove();

                    if( result[x] ){
                        $('.link1.' + x).append('<a href="' + result[x].link1 + '" target="_blank">link1</a>')
                        $('.link2.' + x).append('<a href="' + result[x].link2 + '" target="_blank">link2</a>')
                    }
                }
	    	}
	   	});
    });
});